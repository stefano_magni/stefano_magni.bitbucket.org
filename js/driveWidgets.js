var driveWidgetIdentifier = "data-tt-google-drive-widget";

$("html").on(googleAuthCompleted, function() {
	gapi.client.load('drive', 'v2', createDriveWidgets);
});

function createDriveWidgets(){
	$("div["+driveWidgetIdentifier+"]").each(function(){
		var buildFunctionName = $(this).attr(driveWidgetIdentifier);
		var buildFunction = window[buildFunctionName];
		if (typeof buildFunction === "function"){ 
			buildFunction.apply(null, $(this));
		}
	});
}

function driveFileList(container){
	
	findListWithDetails(gapi.client.drive.files.list, 
						{'userId': "me","maxResults":"10"}, 
						gapi.client.drive.files.get, 
						{'userId': "me"}, 
						"items",
						"fileId",
						function(response){
							buildWidgetHtml(container,response);
						});
		
}