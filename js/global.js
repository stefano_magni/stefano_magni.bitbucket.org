var CLIENT_ID = '459027421948-rb5k3n8ovga7tldea8pi0snf4ugqgt54.apps.googleusercontent.com';
var SCOPES = ['https://www.googleapis.com/auth/gmail.readonly','https://www.googleapis.com/auth/drive.readonly'];

var googleAuthCompleted = "googleAuthCompleted";

function initGoogleApiLibrary(){
    gapi.auth.authorize({'client_id': CLIENT_ID, 
    					 'scope': SCOPES, 
    					 'immediate': true}, 
    					handleGoogleApiAuthResult);
}

function handleGoogleApiAuthResult(authResult){
	
	if (authResult && !authResult.error) {
		$("html").trigger(googleAuthCompleted);
	} else {
	    gapi.auth.authorize({'client_id': CLIENT_ID, 
			 'scope': SCOPES, 
			 'immediate': false}, 
			 handleGoogleApiAuthResult);
    }
	
}

function findListWithDetails(listRequestFunction, listRequestParams, getRequestFunction, getRequestParams, listName, idFieldName, callback){
	
	var listRequest = listRequestFunction(listRequestParams);
	listRequest.execute(function(response){
		
		var batch = gapi.client.newBatch();
		for (var i = 0; i < response[listName].length; i++) {
			getRequestParams[idFieldName] = response[listName][i].id;
			var getRequest = getRequestFunction(getRequestParams);
			batch.add(getRequest);
		}
		
		batch.then(function(response){
			
			var listArray = new Array();
			var keys = Object.keys(response.result);
			for (var i = 0; i < keys.length; i++) {
				listArray[i] = response.result[keys[i]].result;
			}
			
			var listResponse = new Object();
			listResponse.list = listArray;
			
			callback(listResponse);
		});
		
	});
	
}

function buildWidgetHtml(container, context){
	var choosenTemplate = $(container).attr("data-tt-google-template");
	var source  = $("#"+choosenTemplate).html();
	var template = Handlebars.compile(source);
	var html = template(context);
	$(container).html(html);
}