var gmailWidgetIdentifier = "data-tt-google-gmail-widget";

$("html").on(googleAuthCompleted, function() {
	gapi.client.load('gmail', 'v1', createGmailWidgets);
});

function createGmailWidgets(){
	$("div["+gmailWidgetIdentifier+"]").each(function(){
		var buildFunctionName = $(this).attr(gmailWidgetIdentifier);
		var buildFunction = window[buildFunctionName];
		if (typeof buildFunction === "function"){ 
			buildFunction.apply(null, $(this));
		}
	});
}

function gmailMessagesList(container){
	
	findListWithDetails(gapi.client.gmail.users.messages.list, 
						{'userId': "me","maxResults":"10","q":"in:inbox category:primary"}, 
						gapi.client.gmail.users.messages.get, 
						{'userId': "me"}, 
						"messages",
						"id",
						function(response){
							buildWidgetHtml(container,response);
						});
		
}